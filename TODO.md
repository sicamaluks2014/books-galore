# :white_check_mark: ~~build search~~ 
    We want our users to be able to search for items using an input field. The list of items displayed on the app should be determined by the state of the search. This is a stateful component with user input stored in state value called "searchTerm"
# :white_check_mark: ~~build filters~~ 
# :white_check_mark: ~~build book details~~ 
# :white_check_mark: ~~build in pagination~~ 
# :white_check_mark: ~~build dark | light mode~~ 
