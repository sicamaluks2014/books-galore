import { expect, browser, $ } from '@wdio/globals'

const BASEURL = "http://localhost:5173/"
const BASEURLGITLAB = "https://books-galore-sicamaluks2014-952c072c5c7b3fe43410f6011501c7da114.gitlab.io/"

// describe('My Login application', () => {
//     it('should login with valid credentials', async () => {
//         await browser.url(`https://the-internet.herokuapp.com/login`)

//         await $('#username').setValue('tomsmith')
//         await $('#password').setValue('SuperSecretPassword!')
//         await $('button[type="submit"]').click()

//         await expect($('#flash')).toBeExisting()
//         await expect($('#flash')).toHaveTextContaining(
//             'You logged into a secure area!')
//     })
// })

describe('My Book List application', () => {
    const bookIds = [
        "d1b3c101-eb88-46b1-ac50-cd6941472b1a",
        "d42e469d-9b0f-4118-a25b-0ae913527d15",
        "9386a7bc-18d3-4b15-8301-225bead9406e",
        "62565b25-8ab9-41f6-bab6-205e708951af",
        "f9aa249e-a604-46b3-a46e-6711ff6bb66b",
        "36e9e1f5-4c56-4608-9d96-be1f4e0f55db",
        ]    
    
    it('should seach for a book using search input and genre filter', async () => {
        await browser.url(`${BASEURLGITLAB}`)
        await $('input[type="search"]' ).setValue('economics')
        for(const id of bookIds){   
            const book_preview_test_attr = $(`#book-${id}`) 
            const book_details_test_attr = $(`#book-details-${id}`) 
                              
            await expect(book_preview_test_attr).toBeExisting()
            await book_preview_test_attr.click()

            
            await expect(book_details_test_attr).toBeExisting()           
            await $('[data-list-close]').click()
        }
    })
})

describe('Select Element Test', () => {
    it('should select an option from the genre dropdown', async () => {
        await browser.url(`${BASEURL}`)
  
      // Locate the select element
      const selectElement = $('#genre' );
  
      // Use the selectByVisibleText method to select an option by its visible text
      await selectElement.selectByVisibleText('Economics');
  
      // Perform assertions or additional actions based on the selected option
      const selectedOption = await selectElement.getValue();
      expect(selectedOption).toBe('Economics');
  
      // You can also use other methods like selectByAttribute, selectByIndex, etc.
      // Example: selectElement.selectByAttribute('value', 'option3');
    });
  });
