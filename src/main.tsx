import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import ModalContextProvider from './contexts/ModalContext.tsx'
import { BooksContextProvider } from './contexts/BooksContext.tsx'
import { bookCollection } from './lib/data'
import { ThemeContextProvider } from './contexts/ThemeContext'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <ThemeContextProvider>
      <BooksContextProvider  initialBooks={bookCollection}>
        <ModalContextProvider>        
          <App />
        </ModalContextProvider>
      </BooksContextProvider>
    </ThemeContextProvider>
  </React.StrictMode>
)
