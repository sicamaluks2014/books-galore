import './App.css'
import Header from './components/Header';
import BookList from './components/books/BookList';

function App() {    
  return (
    <div className='w-full'>
      <Header />
      <BookList />  
    </div>    
  );
}

export default App
