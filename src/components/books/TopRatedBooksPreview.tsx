import { Book } from '../../lib/types'

function BookPreview( { author, id, title, popularity } : Book) {
  return (
    <div className='flex flex-col items-start cursor-pointer text-left border-2 p-2' key={id}>
      <h3 className=' font-semibold'>{title}</h3>
      <div className='flex justify-between w-full'>
      <p>{author}</p>
        <span>
          <span className='animate-pulse'>&#10084;</span>
          {popularity}
        </span>
      </div>

    </div>
  )
}

export default BookPreview