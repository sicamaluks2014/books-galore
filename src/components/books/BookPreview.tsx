import Skeleton from 'react-loading-skeleton'
import { Book } from '../../lib/types'
import { authors } from '../../lib/data'


function BookPreview( book : Book) { 
  return (
      <>
        <img 
          src={book.image } 
          alt="Book Cover" 
          className='w-[58px] h-[80px] object-cover bg-gray-300 rounded-sm shadow-md pointer-events-none p-2' 
          />
        <section>
          <h3 className=' font-semibold'>{book.title || <Skeleton />}</h3>
          <p>{authors[book.author] || <Skeleton /> }</p>
        </section>
      </> 
  )
}

export default BookPreview