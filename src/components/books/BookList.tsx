import { useBooksContext } from '../../contexts/BooksContext.js'
import { Book } from '../../lib/types.js'
import BookPreview from './BookPreview.js'
import { authors, genres } from '../../lib/data.js'
import Button from '../ui/Button.js'
import Search from '../ui/Search.js'
import Select from '../ui/Select.js'
import Modal from '../ui/Modal.js';
import BookDetail from './BookDetail.js';
import { useModalContext } from '../../contexts/ModalContext.js'
import Top10Books from './Top10Books.js'
import BookListPagination from './BookListPagination.js'
import { Suspense } from 'react'

function BookList() {
    const { books, booksPerPage, currentPage, selectedBook, setSelectedBook } = useBooksContext()
    const {showModal, setShowModal} = useModalContext()
    const bookCount = books.length - booksPerPage

    const indexOfLastItem = currentPage * booksPerPage;
    const indexOfFirstItem = indexOfLastItem - booksPerPage;
    const currentItems = books.slice(indexOfFirstItem, indexOfLastItem);

    const handleBookClick = (book: Book) => {
      setSelectedBook(book);
      setShowModal(true);
    };
  
  return (
    <section className='flex w-[90%] m-auto p-4 gap-8 max-md:flex-col max-sm:justify-center'>
      <div className='w-[65%] max-md:w-full'>
        <section className='flex flex-col gap-8 w-full items-left border-b pb-4 '>
          <h1 className='text-3xl max-md:text-center '>Available books</h1>
          <div className='max-md:hidden'>
            <BookListPagination books={books}/>
          </div>          
        </section>        
        <div className='grid grid-cols-2 gap-4 max-lg:grid-cols-1 max-lg:(flex flex-col items-center)'>
          { currentItems.slice(0, booksPerPage).map(
            (book: Book,  index) => {
              console.log(`book-${book.id}`)
              return (
              <Suspense fallback={<p>loading book...</p>}>
                  <button 
                  className='flex items-center cursor-pointer text-left rounded-md gap-2 p-2 outline shadow-sm m-4 w-full' 
                  id={`book-${book.id}`}
                  key={book.id}
                  onClick={()=>handleBookClick(book)}
                >
                  <BookPreview key={index} {...book} />
                </button>
              </Suspense>  
              ) 
              }             
            )
          }
        </div>        
        <div className='flex p-2 justify-center font-medium'>
          { 
          bookCount > 0 ?  <Button text={`More books... (${bookCount})`} disabled={false} />: <Button text={`More books... (0)`} disabled={true} />
          }
        </div>        
      </div>        
      <div className='flex flex-col gap-4 p-4 border pt-10 w-[30%] max-md:hidden'>
        <section className='w-full'>
          <Search />
        </section>
        <h2 className=' font-semibold text-2xl'>Filter Options</h2>
        <Select options={genres} selected={'All Genres'} />
        {/* <select name="author" id="author" className=' p-2 outline outline-gray-500' onChange={filterByAuthor}>
          <option value="All Authors" selected>All Authors</option>
          {
            Object.keys(authors).map((author: string)=> (<option value={authors[author]}  key={author}>{authors[author]}</option>))
          }         
        </select> */}
        {/* <select name="genre" id="genre" className=' p-2 outline outline-gray-500' onChange={filterByGenre}>
          <option value="All Genres">All Genres</option>
          {
            Object.keys(genres).map((genre: string)=> (<option key={genre} value={genres[genre]}>{genres[genre]}</option>))
          }         
        </select> */}
        <Select options={authors} selected={'All Authors'} />
        <section>
          <h3 className='font-semibold text-2xl'>Top 10 Books</h3>
        </section>
        <Top10Books books ={books}/>
      </div>  
      <Modal isOpen={showModal} >
        <BookDetail {...selectedBook}/>
      </Modal>      
    </section>
  )
}

export default BookList