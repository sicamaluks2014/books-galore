import { useBooksContext } from "../../contexts/BooksContext";
import { useModalContext } from "../../contexts/ModalContext";
import { Book } from "../../lib/types"

function BookDetail(book: Book) {
  const { setShowModal } = useModalContext()
  const { setSelectedBook } = useBooksContext()
  const publishDate = new Date(Date.parse(book.published))

  const handleCloseModal = () => {
    setSelectedBook(null);
    setShowModal(false);
  };
  return (
    <div className="max-sm:w-full m-auto" data-list-active id={`book-details-${book.id}`} >
      <div className="overlay__preview">
        <img className="overlay__blur" data-list-blur src={book.image}/>
        <img className="overlay__image" data-list-image src={book.image}/>
      </div>
      <div className="overlay__content ">
        <h3 className="overlay__title" data-list-title>{book.title}</h3>
        <div className="overlay__data mb-8" data-list-subtitle>Publish Year: {publishDate.getFullYear()}</div>
        <p className="overlay__data overlay__data_secondary" data-list-description>{book.description}</p>
      </div>
      <div className="overlay__row">
        <button className="overlay__button overlay__button_primary dark:text-slate-900" data-list-close onClick={()=>handleCloseModal()}>Close</button>
      </div>
    </div>
  )
}

export default BookDetail