import { useBooksContext } from '../../contexts/BooksContext';
import { BooksProps } from '../../lib/types';

function BookListPagination( props: BooksProps) {
    const { setCurrentPage, booksPerPage} = useBooksContext()

    function paginate(pageNumber: number): void {
      setCurrentPage(pageNumber);
    }
  return (
    <div className='flex gap-8 w-full items-center'>
        <p>Pages:</p>
        {Array.from({ length: Math.ceil(props.books.length / booksPerPage) }).slice(0,10).map((_, index) => (
        <button key={index} onClick={() => paginate(index + 1)} className='flex gap-2 w-full items-center'>
        {index + 1}
        </button>
        ))}
    </div>
  )
}

export default BookListPagination