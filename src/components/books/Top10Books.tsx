import { Authors, Book, BooksProps } from '../../lib/types'
import { authors } from '../../lib/data'
import TopRatedBookPreview from './TopRatedBooksPreview.js'

function Top10Books( props : BooksProps) {
    const sortedBooks = props.books.sort((a, b)=>{
        return b.popularity - a.popularity
      })

      return sortedBooks.slice(0,10).map((book: Book) => <TopRatedBookPreview 
        id={book.id} 
        key={book.id}
        genres={book.genres} 
        title={book.title} 
        image={book.image} 
        description={book.description} 
        pages={book.pages} 
        published={book.published} 
        author={authors[book.author as keyof Authors] } 
        popularity={book.popularity} 
        />
        )
}

export default Top10Books