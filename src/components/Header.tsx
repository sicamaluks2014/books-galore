import { Icon } from '@iconify/react';
import { useThemeContext } from '../contexts/ThemeContext';

function Header() {
  const { darkMode, setDarkMode } = useThemeContext()
  const body = document.querySelector('body')
  const modal = document.querySelector('.modal-overlay')
  darkMode ? body ?.classList.add('night'): body?.classList.remove('night')
  darkMode ? modal?.classList.add('night'): modal?.classList.remove('night')
  return (
    <div className='flex justify-between p-4 text-white bg-slate-900'>
        <h1 className='text-3xl font-bold'>Books Galore</h1>
        <button onClick={() => setDarkMode(!darkMode)}>
          {
            darkMode ? <Icon icon="wi:day-sunny" fontSize={36} />: <Icon icon="ic:baseline-mode-night" fontSize={30} /> 
          }
        </button>
    </div>
  )
}

export default Header