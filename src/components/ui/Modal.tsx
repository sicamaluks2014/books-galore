import { ReactNode } from "react";

const Modal = ({ isOpen, children }: {
    isOpen: boolean,
    children: ReactNode
}) => {
    if (!isOpen) {
      return null;
    }
  
    return (
      <div className="modal-overlay dark:bg-black">
        <div className="modal w-1/2">         
          {children}
        </div>
      </div>
    );
  };
  
export default Modal