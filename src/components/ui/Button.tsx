import { useBooksContext } from "../../contexts/BooksContext"
import { ButtonProps } from "../../lib/types"

function Button({ text, disabled }: ButtonProps) {
    const { booksPerPage, setBooksPerPage } = useBooksContext()
  return (
    <button 
      onClick={()=> setBooksPerPage(booksPerPage+36)} 
      className="outline p-2 rounded-sm outline-blue-600"
      disabled={disabled}
    >
      { text }
    </button>
  )
}

export default Button