import { useEffect, useState } from 'react'
import { useBooksContext } from '../../contexts/BooksContext';
import { useDebounce } from '../../lib/custom-hooks';

function Search() {
    const [searchQuery, setSearchQuery] = useState('');
    const debouncedSearch = useDebounce(searchQuery)
    
    const { searchBooks } = useBooksContext()    
    // const handleSearch = useDebounce(query); 
    
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const query = event.target.value;
        setSearchQuery(query);               
    };
    
    useEffect(()=>{
      const loadBooks = () => {
        searchBooks(debouncedSearch)
      }

      loadBooks()
    }, [debouncedSearch])

    
  return (
    <section>
        <input 
            type='search' 
            className='border-b text-md p-2 focus:outline-blue-600 w-full text-center'
            value={searchQuery}            
            onChange={handleChange} 
            placeholder='Search by title, author, or genre' 
        />
    </section>
  )
}

export default Search



