import { ChangeEvent, useEffect, useState } from 'react'
import { selectMap,  } from '../../lib/types'
import { useBooksContext } from '../../contexts/BooksContext';
import { useDebounce } from '../../lib/custom-hooks';

function Select({options, selected}: selectMap) {
    const [searchQuery, setSearchQuery] = useState('');
    const debouncedSearch = useDebounce(searchQuery)
    const { searchBooks } = useBooksContext()   

    function filterBooks(event: ChangeEvent<HTMLSelectElement>): void {
        let value = event.currentTarget.value
        if( value === "All Genres" || value === "All Authors") { 
            value = '' 
        }
        setSearchQuery(value);
    }

    useEffect(()=>{
        const loadBooks = async() => {
          await searchBooks(debouncedSearch)
        }
  
        loadBooks()
      }, [debouncedSearch])

  return (
    <select name="genre" id="genre" className=' p-2 outline outline-gray-500 dark:text-white dark:bg-gray' onChange={filterBooks}>
        <option value={selected}>{selected}</option>
        {
        Object.keys(options).map((option: string)=> (<option key={option} value={options[option]}>{options[option]}</option>))
        }         
    </select>
  )
}

export default Select

