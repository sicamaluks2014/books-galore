import { createContext, useContext, useState } from "react";
import { ModalContextProviderProps, ModalProps } from "../lib/types";

export const ModalContext = createContext<ModalProps | null>(null)

export default function ModalContextProvider({ children }: ModalContextProviderProps){
    const [showModal, setShowModal] = useState(false)
    return (
        <ModalContext.Provider value={{ showModal, setShowModal}}>
            { children }
        </ModalContext.Provider>
    )
}
export const useModalContext = () => {
    const modalContext = useContext(ModalContext)    
    if(!modalContext){
        throw new Error("Modal context should be used within ModalContextProvider")
    }
    return modalContext

}