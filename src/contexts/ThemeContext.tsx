import { useContext, createContext, useState } from "react";
import { ChildrenProps } from "../lib/types";


export type themeProvider = {
    darkMode: boolean,
    setDarkMode: React.Dispatch<React.SetStateAction<boolean>>//(mode: boolean) => void

}
// Step 1. Create a theme context object by using React.createContext()
export const ThemeContext = createContext<themeProvider | null>(null)
// Step 2. Provide the globally created context to child components using a Provider
export const useThemeContext = ()=>{
    const context = useContext(ThemeContext)
    if(!context){
        throw new Error("ThemeContext must be used within a ThemeContextProvider")
    }
    return context
}

export const ThemeContextProvider = ({children}: ChildrenProps)=>{
    const [ darkMode, setDarkMode ] = useState(false)
    return <ThemeContext.Provider value={{darkMode, setDarkMode}}>
        {children}
    </ThemeContext.Provider>
}



