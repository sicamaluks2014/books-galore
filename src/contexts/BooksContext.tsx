import { ReactNode, createContext, useContext, useState } from "react";
import { Book, BooksContextType } from "../lib/types";
import { authors, genres } from '../lib/data'

const BooksContext = createContext<BooksContextType | null>(null)

export const BooksContextProvider: React.FC<{ children: ReactNode; initialBooks: Book[] }> = ({ children, initialBooks } ) => {
      const [books, setBooks] = useState<Book[]>(initialBooks)
      const [selectedBook, setSelectedBook] = useState<Book>(initialBooks[0]);
      const [booksPerPage, setBooksPerPage] = useState(36)
      const [currentPage, setCurrentPage] = useState(1)

      const searchBooks = async (query: string)=>{
        const filteredBooks = initialBooks.filter((book) => 
              book.title.toLowerCase().includes(query.toLowerCase()) ||
              authors[book.author].toLowerCase().includes(query.toLowerCase())  ||            
              book.genres.includes(Object.keys(genres).find(genre => genres[genre].toLocaleLowerCase() === query.toLowerCase())!)              
          );
          
        setBooks(filteredBooks);
          
      }
     return (
        <BooksContext.Provider value={{books, searchBooks, booksPerPage, setBooksPerPage, currentPage, setCurrentPage, selectedBook, setSelectedBook}}>
            {children}
        </BooksContext.Provider>
     )
}

export const useBooksContext = ()=>{
    const context = useContext(BooksContext)

    if(!context){
        throw new Error("useBooksContext must be used within a BooksContextProvider")
    }

    return context
}