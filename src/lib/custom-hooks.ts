import { useEffect, useState } from "react"
import { Book } from "./types"

export const useDebounce = <T>(value: T, delay=1000)=>{
    const [debouncedValue, setDebouncedValue] = useState<T>(value)
    useEffect(()=>{
     const timeout = setTimeout(()=>{
        setDebouncedValue(value)}, 
      delay)

     return () => clearTimeout(timeout)
    }, [value, delay])

    return debouncedValue
}

let status: string = "pending";
let result: Book[] ;

export function useDataFetch(url: string) {
    let fetching = fetch(url)
    .then((res) => res.json())
    // Fetch request has gone well
    .then((success) => {
      status = "fulfilled";
      result = success;
    })
    // Fetch request has failed
    .catch((error) => {
      status = "rejected";
      result = error;
    });

  return () => {
    if (status === "pending") {
        console.log("fetching...")
      throw fetching; // Suspend(A way to tell React data is still fetching)
    } else if (status === "rejected") {
      throw result; // Result is an error
    } else if (status === "fulfilled") {
      return result; // Result is a fulfilled promise
    }
  };
}