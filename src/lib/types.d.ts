import { nanoid } from 'nanoid'

export type Authors = {
    [key: string]: string;
}

export type Genres = {
    [key: string]: string;
}
export type Book = {
    id: string;
    genres: string[];
    popularity: number;
    title: string;
    image: string;
    description: string;
    pages: number;
    published: string;
    author: string
}

export type ButtonProps = {
    text: string;
    disabled: boolean;
}

export type BooksProps = {
    books : Book[]
}
export type BooksContextType = {
    books: Book[];
    booksPerPage: number;
    currentPage: number;
    selectedBook: Book;
    setSelectedBook: React.Dispatch<React.SetStateAction<Action>>    
    setCurrentPage: (num)=> void;    
    setBooksPerPage: (num)=> void;
    searchBooks: (query: string) => void;
}

export type ChildrenProps = {
    children: ReactNode;   
}

export export type ModalProps = {
    showModal: ModalStatus,
    setShowModal: React.Dispatch<React.SetStateAction<ModalStatus>>
}

export export type SearchProps = {
    search: string,
    setSearch: React.Dispatch<React.SetStateAction<string>>
}
type ModalContextProviderProps = {
    children: ReactNode
}

export type FormProps = {
    onSubmit: (data: FormData) => void;
  }

export type selectMap = {
    options: {[key: string]:string};
    selected: string
}